# WUST Ground Station

Ground station project realized at the Wrocław University of Science and
Technology as part of a team project in the summer semester 2023/24.

## 1. Project description.

The aim of the project is to build a ground station capable of receiving messages from satellites placed in low Earth orbit. The starting point will be the TinyGS project, because it is currently the easiest and cheapest solution to implement such communication. Estimated project result is to have working ground station that will be able to send and recive messages from satellite created by second team. The station will be able to predict time window to contact with satellite and all of HMI will be placed on a website for easier control. The project develops problem with robot communication such as satellite on a large distance and teaches how to set a connection with it using least given resources.

The project's source code, documentation and diagrams will be published on Gitlab on a public repository in this group.


## 2. Milestones delivery time scheme

1. Definition of project objectives and creating demonstrative version for every module.  
    **Deadline:** 26.04.2024  
    **Veryfication:** Rotor exists and moves. LoRa module is embedded on a PCB board with simple program to communicate via protocol using simulation. Computer is able to predict satelite position using TLE standard. Communication protocol exists and frame decoding works using simple simulation. Website with grafana and influx exists and can showcase simple information. Website that represent simple UI to control station exists and user can interact with it.

2. Advanced stage of every module separetly or first interaction. Every module passes tests that will be set in the future.  
    **Deadline:** 24.05.2024  
    **Veryfication:** Rotor moves and is fully controllable. LoRa module is embedded on a PCB board with program that uses TinyGS and can communicate with advanced simulation. Computer is able to predict when to communicate with satellite, and knows how to communicate thorugh simulation. Website with Grafana and InfluxDB can communicate with website application that user can interact with and recive information from Graphana and InfluxDB.

3. Establishing connection with a Satellite team and completion of communication for every module.  
    **Deadline:** 14.06.2024  
    **Veryfication:** Ground station communicates with satellite through working rotor. Everything can be controlled and read using website application by user. All information from Grafana and Influx is available on website. Communication with simulated satellite is available. All modules are working together.

## 3. Budget

The budget will be several thousand zlotys. Detailed data will be developed on the basis of other projects such as the one below:
https://blog.wsoltysiak.pl/tinygs-twoja-wlasna-naziemna-stacja-satelitarna/

## 4. Work management

The project will be implemented by a student group in cooperation with Tarnowski.io. The group will work as a whole, but individual components will have a designated person responsible for them. The project will be managed on Gitlab. The repositories will include tasks assigned to individual group members. The communication channel will be the Discord group. The group leader is responsible for coordinating the project. Scrum agile team collaboration framework will be used to coordine work management. We will have 2 around 40 minuts meetings per week to synchronize work and talk about obstacles. Of course if the situation will say so we can change frequentcy of the meetings. We will use meeting system called Sprint (one per month) to precisely set goals for next month. 

Any conflicts that will occur during project developement will be solved using democracy. If the conflict will occur we will vote for options and the option that will have most votes will win. If the draw occurs the deciding vote will be given to the Juliusz as a project manager.

The group will cooperate with a twin project implemented as part of the same university classes, under which a nanosatellite is being created.

Unless otherwise noted, all projects included in this group are under the
[MIT License](https://choosealicense.com/licenses/mit/).
This may change if project members deem it beneficial.

## 5. Authors

Team Project Members:

1. [**Mikołaj Bajor**]() - **Skills**: Maths and telecommunication skills **Resposible for:** Communication protocol with satellite and frame decoding.
2. [**Joanna Chrobot**]() - **Skills**: Embedded programming, **Resposible for:** TinyGS module based on LoRa module
3. [**Hanna Grześkowiak**]() - **Skills**: Software developer, **Resposible for:**  Station data agregation to server using Influx, Grafana
4. [**Michał Mastej (Team Leader)**](https://gitlab.com/Mastej-Git) - **Skills**: Front-End, Back-End developer, **Resposible for:** Application to plan contact with satellite and recive messages
5. [**Marcin Poźniak**]() - **Skills**: Software developer, Math skills **Resposible for:** Prediction of satellite visibility based on TLE
6. [**Adam Wąs**]() - **Skills**: Mechanic, embedded programming, electronics **Resposible for:** Rotor - launching and integration with LoRa module

Substantive help:

1. [**Juliusz Tarnowski**](https://tarnowski.io) - project manager
